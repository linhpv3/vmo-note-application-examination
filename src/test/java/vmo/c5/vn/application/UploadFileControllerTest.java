package vmo.c5.vn.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import vmo.c5.vn.application.service.UploadFileService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class UploadFileControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@BeforeEach
	public void beforeTest() {
		
	}
	
	@MockBean
	UploadFileService uploadFileService;
	
	
}
