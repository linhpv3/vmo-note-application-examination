package vmo.c5.vn.application;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import vmo.c5.vn.application.controller.request.NoteRequestDTO;
import vmo.c5.vn.application.controller.response.NoteResponse;
import vmo.c5.vn.application.dto.CheckBox;
import vmo.c5.vn.application.dto.Image;
import vmo.c5.vn.application.dto.NoteDTO;
import vmo.c5.vn.application.entity.Note;
import vmo.c5.vn.application.entity.User;
import vmo.c5.vn.application.exception.BusinessException;
import vmo.c5.vn.application.repository.NoteRepository;
import vmo.c5.vn.application.repository.UserRepository;
import vmo.c5.vn.application.service.NoteService;
import vmo.c5.vn.application.utils.NoteStatus;
import vmo.c5.vn.application.utils.NoteType;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class NoteControllerTest {
	
	@Mock(answer = Answers.CALLS_REAL_METHODS)
	NoteService noteService;

	@MockBean
	NoteRepository noteRepository;
	
	@MockBean
	UserRepository userRepository;
	
	@Autowired
	MockMvc mockMvc;
	
	private String API_URL = "/api/v1/notes/";
	
	private ObjectMapper objectMapper;
	
	@BeforeEach
	public void beforeTest() {
		objectMapper = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Test
	public void testFindAll() throws Exception {
		Integer userId = 1;
		Mockito.when(noteService.getNotes(userId)).thenReturn(getAll());

		mockMvc.perform(get(API_URL + "all/" + userId)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.hasSize(3)))
				.andExpect(jsonPath("$[0].title", Matchers.is("title")))
				.andExpect(jsonPath("$[1].title", Matchers.is("title1")));
	}
	
	@Test
	public void testAmountNoteUncomplete() throws Exception {
		Integer userId = 1;
		Mockito.when(noteService.amountUncompleteNote(userId)).thenReturn(3);

		mockMvc.perform(get(API_URL + "amount-uncomplete/" + userId)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("3"));
	}
	
	@Test
	public void testFindOne() throws Exception {
		
		Integer id = 1;
		Mockito.when(noteService.getNoteId(id)).thenReturn(getOne());

		mockMvc.perform(get(API_URL + id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title", Matchers.is("title")))
				.andExpect(jsonPath("$.id", Matchers.is(id)))
				.andExpect(jsonPath("$.description", Matchers.is("description")))
				.andExpect(jsonPath("$.status", Matchers.is(NoteStatus.COMPLETE.toString())));
	}
	
	@Test
	public void testFindOneNotFound() throws Exception {
		
		Integer id = 2;
		Mockito.when(noteService.getNoteId(id)).thenThrow(BusinessException.class);

		mockMvc.perform(get(API_URL + id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void testInsertValid() throws Exception {
		
		NoteDTO noteDTO = new NoteDTO("title", "description", NoteStatus.INCOMPLETE, 1);
		
		NoteResponse noteResponse = NoteResponse.builder()
				.Id(1)
				.title("title")
				.description("description")
				.status(NoteStatus.INCOMPLETE)
				.type(NoteType.NORMAL)
				.build();
		
		NoteRequestDTO noteRequestDTO = new NoteRequestDTO();
		noteRequestDTO.setTitle("title");
		noteRequestDTO.setUserId(1);
		noteRequestDTO.setStatus(NoteStatus.INCOMPLETE);
		noteRequestDTO.setType(NoteType.NORMAL);
		noteRequestDTO.setDescription("description");
		
		Mockito.when(noteService.createNote(noteDTO)).thenReturn(noteResponse);
		
		mockMvc.perform(post(API_URL)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(noteRequestDTO))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", Matchers.is(1)))
				.andExpect(jsonPath("$.title", Matchers.is("title")))
				.andExpect(jsonPath("$.description", Matchers.is("description")))
				.andExpect(jsonPath("$.type", Matchers.is(NoteType.NORMAL.toString())))
				.andExpect(jsonPath("$.status", Matchers.is(NoteStatus.INCOMPLETE.toString())));
	}
	
	@Test
	public void testUpdateValid() throws Exception {
		
		Integer updateId = 1;
		NoteDTO noteDTO = new NoteDTO("title1", "description1", NoteStatus.INCOMPLETE, 1);
		noteDTO.setId(updateId);
		
		NoteResponse noteResponse = NoteResponse.builder()
				.Id(updateId)
				.title("title1")
				.description("description1")
				.status(NoteStatus.COMPLETE)
				.type(NoteType.NORMAL)
				.build();
		
		NoteRequestDTO noteRequestDTO = new NoteRequestDTO();
		noteRequestDTO.setTitle("title1");
		noteRequestDTO.setUserId(updateId);
		noteRequestDTO.setStatus(NoteStatus.COMPLETE);
		noteRequestDTO.setType(NoteType.NORMAL);
		noteRequestDTO.setDescription("description1");
		
		User user = new User();
		user.setId(updateId);
		
		Note note = new Note(updateId, "title1", "description1", NoteType.NORMAL, "", NoteStatus.INCOMPLETE, user);
		note.setId(updateId);
		
		Mockito.mock(NoteService.class, Mockito.CALLS_REAL_METHODS);
		Mockito.when(noteService.updateNote(noteDTO)).thenCallRealMethod();
		Mockito.when(noteRepository.findById(1)).thenReturn(Optional.of(note));
		Mockito.when(userRepository.findById(updateId)).thenReturn(Optional.of(user));
		Mockito.when(noteRepository.save(note));
		
		
		mockMvc.perform(put(API_URL + updateId)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(noteRequestDTO))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testDeleteValid() throws Exception {
		Integer id = 1;
		
		Mockito.when(noteService.deleteNote(id)).thenReturn(1);
		
		mockMvc.perform(delete(API_URL + id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());
	}
	
	
	private List<NoteResponse> getAll() {
		List<NoteResponse> noteResponses = new ArrayList<>();
		NoteResponse noteResponse = NoteResponse.builder().Id(1).title("title").description("description")
				.status(NoteStatus.COMPLETE).type(NoteType.NORMAL).build();

		noteResponses.add(noteResponse);

		Image image = new Image();
		image.setName("test");
		image.setUrl("http://127.0.0.1:8090/note/image/IMG_1659327978747_1659327988780.jpg");
		noteResponse = NoteResponse.builder().Id(2).title("title1").description("description2")
				.status(NoteStatus.COMPLETE).type(NoteType.IMAGE).image(image).build();
		noteResponses.add(noteResponse);

		List<CheckBox> checkboxes = new ArrayList<>();

		noteResponse = NoteResponse.builder().Id(3).title("title2").description("description2")
				.status(NoteStatus.COMPLETE).type(NoteType.MULTI_CHECKBOX).checkboxes(checkboxes).build();
		noteResponses.add(noteResponse);

		return noteResponses;
	}
	
	private NoteResponse getOne() {
		return NoteResponse.builder()
		.Id(1)
		.title("title")
		.description("description")
		.status(NoteStatus.COMPLETE)
		.type(NoteType.NORMAL)
		.build();
	}

}
