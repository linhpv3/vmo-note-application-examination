package vmo.c5.vn.application.utils;

import java.net.URL;

public final class CommonUtils {

	/**
	 * check is valid url str
	 * 
	 * @param url
	 * @return
	 */
	public static boolean isValidUrl(String url) {
		try {
			new URL(url).toURI();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Remove \\\\ chars from json string was escaped
	 * 
	 * @param jsonEscape
	 * @return
	 */
	public static String toJsonStr(String jsonEscape) {
		String stringEscape = jsonEscape.replaceAll("\\\\(?=[\\\"])", "");
		String jsonData = stringEscape.charAt(0) == '"' ? stringEscape.substring(1, stringEscape.length() - 1)
				: stringEscape;

		return jsonData;
	}
}
