package vmo.c5.vn.application.controller.request;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import vmo.c5.vn.application.dto.CheckBox;
import vmo.c5.vn.application.dto.Image;
import vmo.c5.vn.application.dto.NoteDTO;
import vmo.c5.vn.application.dto.NoteDTOFactory;
import vmo.c5.vn.application.exception.BusinessException;
import vmo.c5.vn.application.utils.CommonUtils;
import vmo.c5.vn.application.utils.NoteStatus;
import vmo.c5.vn.application.utils.NoteType;

@Data
@NoArgsConstructor
public class NoteRequestDTO {

	private Integer Id;

	@NotEmpty
	private String title;

	private String description;

	@NotNull
	private NoteType type;

	private Image image;

	private List<CheckBox> checkboxes;

	@NotNull
	private NoteStatus status;

	@NotNull
	private Integer userId;

	public NoteDTO toNoteDTO() {

		if (this.type.equals(NoteType.IMAGE)) {
			if (this.getImage() == null) {
				throw new BusinessException("Image can not be null");
			}
			validateImageProperties(image);
		} else if (this.type.equals(NoteType.MULTI_CHECKBOX)) {
			if (this.getCheckboxes() == null) {
				throw new BusinessException("checkboxes can not be null");
			}
			validateCheckboxes(checkboxes);
		}

		return NoteDTOFactory.getNoteDTO(this.type, this);
	}
	
	/**
	 * Image name should readable
	 * @param image
	 */
	private void validateImageProperties(Image image) {
		final String PATTERN = "^\\s*[A-Za-z0-9\\s_]*\\s*$";
		if (!image.getName().matches(PATTERN)) {
			throw new BusinessException("Image name just have this characters A-Za-z0-9 and space");
		}

		if (!CommonUtils.isValidUrl(image.getUrl())) {
			throw new BusinessException("Image url is not valid");
		}

	}
	
	/**
	 * for reading, we just allow all character are not \\
	 * @param checkboxes
	 */
	private void validateCheckboxes(List<CheckBox> checkboxes) {
		final String PATTERN = "^\\s*[^\\/]*\\s*$";

		for (CheckBox checkbox : checkboxes) {
			if (!checkbox.getContent().matches(PATTERN)) {
				throw new BusinessException("checkbox content just have this characters A-Za-z0-9 and space");
			}
		}
	}
}
