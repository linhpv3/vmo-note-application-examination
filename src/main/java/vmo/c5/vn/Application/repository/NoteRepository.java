package vmo.c5.vn.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vmo.c5.vn.application.entity.Note;
import vmo.c5.vn.application.utils.NoteStatus;

@Repository
public interface NoteRepository extends CrudRepository<Note, Integer>, JpaRepository<Note, Integer>{
	
	@Query("SELECT COUNT(n) FROM Note n WHERE n.user.Id=:userId AND n.status = :status")
	Integer amountNoteByStatus(@Param("userId") Integer userId, @Param("status") NoteStatus status);
	
	@Query("SELECT n FROM Note n WHERE n.user.Id=:userId")
	List<Note> findAllByUserId(@Param("userId") Integer userId);
}
