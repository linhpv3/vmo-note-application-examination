package vmo.c5.vn.application.utils;

public enum NoteStatus {
	INCOMPLETE("INCOMPLETE"), 
	COMPLETE("COMPLETE");

	private String status;

	NoteStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}
}
