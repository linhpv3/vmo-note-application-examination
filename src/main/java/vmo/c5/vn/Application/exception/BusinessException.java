package vmo.c5.vn.application.exception;

import java.util.ArrayList;
import java.util.List;

public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String errorMessage;
	private final Object[] params;
	private final List<ErrorDetail> errorDetails;
	private final ErrorDetail errorDetail;

	public BusinessException(String errorMessage) {
		this.errorMessage = errorMessage;
		this.params = null;
		this.errorDetails = new ArrayList<>();
		this.errorDetail = null;
	}

	public BusinessException(String errorMessage, Object... params) {
		this.errorMessage = errorMessage;
		this.params = params;
		this.errorDetails = new ArrayList<>();
		this.errorDetail = null;
	}

	public BusinessException(List<ErrorDetail> errorDetails) {

		this.errorMessage = null;
		this.params = null;
		this.errorDetails = errorDetails;
		this.errorDetail = null;
	}

	public BusinessException(ErrorDetail errorDetail) {
		this.errorMessage = null;
		this.params = null;
		this.errorDetails = new ArrayList<>();
		this.errorDetail = errorDetail;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Object[] getParams() {
		return params;
	}

	public List<ErrorDetail> getErrorDetails() {
		return errorDetails;
	}

	public ErrorDetail getErrorDetail() {
		return errorDetail;
	}
}
