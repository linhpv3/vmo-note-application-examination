package vmo.c5.vn.application.middleware.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import vmo.c5.vn.application.service.NoteService;
import vmo.c5.vn.application.service.impl.NoteServiceImpl;
import vmo.c5.vn.application.utils.WebSocketConstants;

@Controller
public class WebSocketMessageToClientController {

	private Logger logger = LoggerFactory.getLogger(WebSocketMessageToClientController.class);

	@Autowired
	private NoteService noteService;

	// http://kojotdev.com/2019/07/using-spring-websocket-stomp-application-with-vue-js/
	@MessageMapping(WebSocketConstants.CLIENT_SEND_TO_PATH)
	@SendTo(WebSocketConstants.ENDPOINT_CLIENT_PATH)
	public Map<String, Object> send(Integer userId) throws Exception {

		logger.debug("Start send messsage");

		Integer amountUncomplete = noteService.amountUncompleteNote(userId);
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("amountUncomplete", amountUncomplete);

		logger.debug("End send messsage");

		return res;
	}
}
