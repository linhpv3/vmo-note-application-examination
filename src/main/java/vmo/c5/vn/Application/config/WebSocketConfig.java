package vmo.c5.vn.application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import vmo.c5.vn.application.utils.Constants;
import vmo.c5.vn.application.utils.WebSocketConstants;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker(WebSocketConstants.SIMPLE_BROKER_PATH);
		config.setApplicationDestinationPrefixes(WebSocketConstants.DESTINATION_PREFIX_PATH);
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(WebSocketConstants.STOMP_ENPOINT_PATH);
		registry.addEndpoint(WebSocketConstants.STOMP_ENPOINT_PATH)
		.setAllowedOrigins(Constants.CLIENT_VUE_URL)
		.withSockJS();
	}
}
