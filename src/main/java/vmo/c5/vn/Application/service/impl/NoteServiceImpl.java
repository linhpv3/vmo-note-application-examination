package vmo.c5.vn.application.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vmo.c5.vn.application.controller.response.NoteResponse;
import vmo.c5.vn.application.dto.CheckBox;
import vmo.c5.vn.application.dto.Image;
import vmo.c5.vn.application.dto.ImageNoteDTO;
import vmo.c5.vn.application.dto.MultiCheckBoxNoteDTO;
import vmo.c5.vn.application.dto.NoteDTO;
import vmo.c5.vn.application.entity.Note;
import vmo.c5.vn.application.entity.User;
import vmo.c5.vn.application.exception.BusinessException;
import vmo.c5.vn.application.repository.NoteRepository;
import vmo.c5.vn.application.repository.UserRepository;
import vmo.c5.vn.application.service.NoteService;
import vmo.c5.vn.application.utils.CommonUtils;
import vmo.c5.vn.application.utils.NoteStatus;
import vmo.c5.vn.application.utils.NoteType;

@Service
public class NoteServiceImpl implements NoteService {

	private Logger logger = LoggerFactory.getLogger(NoteServiceImpl.class);

	@Autowired
	private NoteRepository noteRepository;

	@Autowired
	private UserRepository userRepository;

	private final String NOTE_MESSAGE_ERROR = "Note not found";

	private final String USER_MESSAGE_ERROR = "User not found";

	@Override
	public NoteResponse getNoteId(Integer noteId) {
		Optional<Note> noteOp = noteRepository.findById(noteId);

		if (noteOp.isEmpty()) {
			throw new BusinessException(NOTE_MESSAGE_ERROR);
		}

		return toNoteResponse(noteOp.get());
	}

	@Override
	public List<NoteResponse> getNotes(Integer userId) {
		List<Note> notes = new ArrayList<>();
		Iterable<Note> noteList = noteRepository.findAllByUserId(userId);
		noteList.forEach(n -> notes.add(n));

		return notes.stream().map(note -> {
			return toNoteResponse(note);
		}).collect(Collectors.toList());
	}

	@Override
	public NoteResponse updateNote(NoteDTO noteDTO) {

		Optional<Note> noteUp = noteRepository.findById(noteDTO.getId());

		if (noteUp.isEmpty()) {
			throw new BusinessException(NOTE_MESSAGE_ERROR);
		}

		Optional<User> userOp = userRepository.findById(noteDTO.getUserId());
		if (userOp.isEmpty()) {
			throw new UsernameNotFoundException(USER_MESSAGE_ERROR);
		}

		Note note = noteUp.get();

		note.setTitle(noteDTO.getTitle());
		note.setDescription(noteDTO.getDescription());
		note.setStatus(noteDTO.getStatus());
		note.setUser(userOp.get());
		note.setType(checkType(noteDTO));
		note.setExtendData(getExtendData(noteDTO));

		Note noteUpdate = noteRepository.save(note);

		return toNoteResponse(noteUpdate);
	}

	@Override
	public NoteResponse createNote(NoteDTO noteDTO) {
		Optional<User> userOp = userRepository.findById(noteDTO.getUserId());
		if (userOp.isEmpty()) {
			throw new UsernameNotFoundException(USER_MESSAGE_ERROR);
		}
		String extendData = getExtendData(noteDTO);
		Note note = new Note();

		note.setId(noteDTO.getId());
		note.setTitle(noteDTO.getTitle());
		note.setDescription(noteDTO.getDescription());
		note.setStatus(noteDTO.getStatus());
		note.setUser(userOp.get());
		note.setType(checkType(noteDTO));
		note.setExtendData(extendData);

		Note noteCreate = noteRepository.save(note);

		return toNoteResponse(noteCreate);
	}

	@Override
	public Integer deleteNote(Integer noteId) {
		Optional<Note> noteUp = noteRepository.findById(noteId);
		if (noteUp.isEmpty()) {
			throw new BusinessException(NOTE_MESSAGE_ERROR);
		}

		noteRepository.deleteById(noteId);
		return 1;
	}

	@Override
	public Integer amountUncompleteNote(Integer userId) {
		return noteRepository.amountNoteByStatus(userId, NoteStatus.INCOMPLETE);
	}

	private NoteResponse toNoteResponse(Note note) {

		NoteResponse noteResponse = NoteResponse.builder().Id(note.getId()).title(note.getTitle())
				.description(note.getDescription()).status(note.getStatus()).type(note.getType()).build();

		if (note.getExtendData() == null) {
			return noteResponse;
		}

		ObjectMapper objectMapper = constructObjectMapper();
		try {
			String jsonData = CommonUtils.toJsonStr(note.getExtendData());

			if (note.getType().getType().equals(NoteType.IMAGE.getType())) {
				noteResponse.setImage(objectMapper.readValue(jsonData, Image.class));
			} else if (note.getType().getType().equals(NoteType.MULTI_CHECKBOX.getType())) {
				noteResponse.setCheckboxes(objectMapper.readValue(jsonData, new TypeReference<List<CheckBox>>() {
				}));
			}
		} catch (JsonMappingException e) {
			logger.error("error ", e);
		} catch (JsonProcessingException e) {
			logger.error("error ", e);
		}

		return noteResponse;
	}

	private NoteType checkType(NoteDTO noteDTO) {
		return noteDTO instanceof ImageNoteDTO ? NoteType.IMAGE
				: noteDTO instanceof MultiCheckBoxNoteDTO ? NoteType.MULTI_CHECKBOX : NoteType.NORMAL;
	}

	private String getExtendData(NoteDTO noteDTO) {

		ObjectMapper objectMapper = constructObjectMapper();

		String jsonData = null;

		if (noteDTO instanceof ImageNoteDTO) {
			try {
				jsonData = objectMapper.writeValueAsString(((ImageNoteDTO) noteDTO).getImage());
			} catch (JsonProcessingException e) {
				logger.error("error ", e.getMessage());
			}
		} else if (noteDTO instanceof MultiCheckBoxNoteDTO) {
			try {
				jsonData = objectMapper.writeValueAsString(((MultiCheckBoxNoteDTO) noteDTO).getCheckboxes());
			} catch (JsonProcessingException e) {
				logger.error("error ", e.getMessage());
			}
		}

		return jsonData;
	}

	private ObjectMapper constructObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return mapper;
	}

}
