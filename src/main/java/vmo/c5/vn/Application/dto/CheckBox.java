package vmo.c5.vn.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckBox {
	
	private Boolean isCheck;
	
	private String content;
}
