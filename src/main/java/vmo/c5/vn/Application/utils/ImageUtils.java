package vmo.c5.vn.application.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ImageUtils {
	
	private final static Logger logger = LoggerFactory.getLogger(ImageUtils.class);

	public static String WORKING_DIR = System.getProperty("user.dir") + "/note/image/";
	
	public static String WEB_PATH_DIR1 = "/note/";

	public static String WEB_PATH_DIR = "/note/image/";
	
	/**
	 * Check file is image file or not
	 * @param file
	 * @return
	 */
	public static boolean isImageFile(File file) {
		Path path = file.toPath();
		String mimeType;
		try {
			mimeType = Files.probeContentType(path);
			if (mimeType != null && mimeType.startsWith("image/")) {
				// It's an image.
				return true;
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		return false;
	}

}
