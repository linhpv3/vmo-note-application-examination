package vmo.c5.vn.application.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import vmo.c5.vn.application.utils.NoteStatus;

@Data
@EqualsAndHashCode(callSuper = true)
public class MultiCheckBoxNoteDTO extends NoteDTO {

	private List<CheckBox> checkboxes;

	public MultiCheckBoxNoteDTO(Integer Id, String title, String description, NoteStatus status, Integer userId,
			List<CheckBox> checkboxes) {
		super(title, description, status, userId);
		this.Id = Id;
		this.checkboxes = checkboxes;
	}

	public static MultiCheckboxDTOBuilder builder(String title, NoteStatus status, Integer userId) {
		return new MultiCheckboxDTOBuilder(title, status, userId);
	}

	public static class MultiCheckboxDTOBuilder extends NoteDTOBuilder {

		public MultiCheckboxDTOBuilder(String title, NoteStatus status, Integer userId) {
			super(title, status, userId);
		}

		private List<CheckBox> checkboxes;

		@Override
		public MultiCheckboxDTOBuilder withId(Integer Id) {
			this.Id = Id;
			return this;
		}

		public MultiCheckboxDTOBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public MultiCheckboxDTOBuilder withMultiCheckBox(List<CheckBox> checkboxes) {
			this.checkboxes = checkboxes;
			return this;
		}

		@Override
		public MultiCheckBoxNoteDTO build() {
			return new MultiCheckBoxNoteDTO(Id, title, description, status, userId, this.checkboxes);
		}
	}
}
