package vmo.c5.vn.application.controller.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;
import vmo.c5.vn.application.dto.CheckBox;
import vmo.c5.vn.application.dto.Image;
import vmo.c5.vn.application.utils.NoteStatus;
import vmo.c5.vn.application.utils.NoteType;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class NoteResponse {
	
	private Integer Id;
	
	private String title;
	
	private String description;

	private NoteType type;

	private NoteStatus status;
	
	private Image image;
	
	private List<CheckBox> checkboxes; 
}
