package vmo.c5.vn.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

//config static resources:  https://stackoverflow.com/questions/41691770/spring-boot-unable-to-serve-static-image-from-resource-folder
//                          https://www.baeldung.com/spring-mvc-static-resources
//                          https://docs.spring.io/spring-framework/docs/5.2.4.RELEASE/spring-framework-reference/core.html#resources-resourceloader
//                          https://stackoverflow.com/questions/21123437/how-do-i-use-spring-boot-to-serve-static-content-located-in-dropbox-folder
@Configuration
public class NoteWebMvcConfig extends WebMvcConfigurationSupport {

	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
			// "classpath:/resources/", --> it use when we not custom in classpath, if
			// config external static serve, turn of this.
			"file:/" + System.getProperty("user.dir") + "/",
			// "file:/C:/Users/Admin/Documents/vmo-note-application-be/",
			// ImageUtils.WEB_PATH_DIR1
	};

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

}
