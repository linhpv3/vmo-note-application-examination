package vmo.c5.vn.application.utils;

public enum NoteType {
	NORMAL("NORMAL"), 
	MULTI_CHECKBOX("MULTI_CHECKBOX"), 
	IMAGE("IMAGE");

	private String type;

	NoteType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
