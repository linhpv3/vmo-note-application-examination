package vmo.c5.vn.application.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import vmo.c5.vn.application.utils.NoteStatus;

@Data
@EqualsAndHashCode(callSuper = true)
public class ImageNoteDTO extends NoteDTO {

	private Image image;

	public ImageNoteDTO(Integer Id, String title, String description, NoteStatus status, Integer userId, Image image) {
		super(title, description, status, userId);
		this.Id = Id;
		this.image = image;
	}

	public static ImageDTOBuilder builder(String title, NoteStatus status, Integer userId) {
		return new ImageDTOBuilder(title, status, userId);
	}

	public static class ImageDTOBuilder extends NoteDTOBuilder {

		public ImageDTOBuilder(String title, NoteStatus status, Integer userId) {
			super(title, status, userId);
		}

		private Image image;

		public ImageDTOBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		@Override
		public ImageDTOBuilder withId(Integer Id) {
			this.Id = Id;
			return this;
		}

		public ImageDTOBuilder withImage(Image image) {
			this.image = image;
			return this;
		}

		@Override
		public ImageNoteDTO build() {
			return new ImageNoteDTO(Id, title, description, status, userId, image);
		}
	}

}
