package vmo.c5.vn.application.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vmo.c5.vn.application.controller.request.NoteRequestDTO;
import vmo.c5.vn.application.controller.response.NoteResponse;
import vmo.c5.vn.application.service.NoteService;

@RestController
@RequestMapping("/api/v1/notes/")
public class NoteController {

	@Autowired
	private NoteService noteService;

	@GetMapping("amount-uncomplete/{userId}")
	public ResponseEntity<Integer> amountNoteUncomplete(@PathVariable("userId") Integer userId) {
		return ResponseEntity.ok(noteService.amountUncompleteNote(userId));
	}

	@GetMapping("all/{userId}")
	public ResponseEntity<List<NoteResponse>> getAllNotes(@PathVariable("userId") Integer userId) {
		List<NoteResponse> response = noteService.getNotes(userId);
		return ResponseEntity.ok(response);
	}

	@GetMapping("{noteId}")
	public ResponseEntity<NoteResponse> getNotebyId(@PathVariable("noteId") Integer noteId) {
		NoteResponse response = noteService.getNoteId(noteId);
		return ResponseEntity.ok(response);
	}

	@PutMapping("{noteId}")
	public ResponseEntity<NoteResponse> updateNotebyId(@PathVariable("noteId") Integer noteId,
			@Valid @RequestBody NoteRequestDTO noteRequestDTO) {
		noteRequestDTO.setId(noteId);
		NoteResponse response = noteService.updateNote(noteRequestDTO.toNoteDTO());
		return ResponseEntity.ok(response);
	}

	@PostMapping()
	public ResponseEntity<NoteResponse> createNote(@Valid @RequestBody NoteRequestDTO noteRequestDTO) {
		NoteResponse response = noteService.createNote(noteRequestDTO.toNoteDTO());
		return ResponseEntity.ok(response);
	}

	@DeleteMapping("{noteId}")
	public ResponseEntity<Integer> deleteNoteById(@PathVariable("noteId") Integer noteId) {
		Integer response = noteService.deleteNote(noteId);
		return ResponseEntity.ok(response);
	}
}
