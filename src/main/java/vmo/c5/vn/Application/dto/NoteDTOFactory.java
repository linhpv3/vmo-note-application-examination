package vmo.c5.vn.application.dto;

import vmo.c5.vn.application.controller.request.NoteRequestDTO;
import vmo.c5.vn.application.utils.NoteType;

public final class NoteDTOFactory {
	
	public static NoteDTO getNoteDTO(NoteType noteType, NoteRequestDTO requestNoteDTO) {
		
		if (noteType == null) {
			return null;
		}

		if (noteType.equals(NoteType.NORMAL)) {
			return NoteDTO.builder( 
					requestNoteDTO.getTitle(), 
					requestNoteDTO.getStatus(),
					requestNoteDTO.getUserId())
					.withId(requestNoteDTO.getId())
					.withDescription(requestNoteDTO.getDescription())
					.build();
		} else if (noteType.equals(NoteType.IMAGE)) {
			return ImageNoteDTO.builder( 
					requestNoteDTO.getTitle(), 
					requestNoteDTO.getStatus(),
					requestNoteDTO.getUserId())
					.withImage(requestNoteDTO.getImage())
					.withDescription(requestNoteDTO.getDescription())
					.withId(requestNoteDTO.getId())
					.build();
		} else if (noteType.equals(NoteType.MULTI_CHECKBOX)) {
			return MultiCheckBoxNoteDTO
					.builder(
					requestNoteDTO.getTitle(), 
					requestNoteDTO.getStatus(),
					requestNoteDTO.getUserId())
					.withId(requestNoteDTO.getId())
					.withDescription(requestNoteDTO.getDescription())
					.withMultiCheckBox(requestNoteDTO.getCheckboxes())
					.build();
		}

		return null;
	}
}
