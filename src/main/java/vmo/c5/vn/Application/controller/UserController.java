package vmo.c5.vn.application.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vmo.c5.vn.application.controller.request.UserRequestDTO;
import vmo.c5.vn.application.controller.response.UserResponse;
import vmo.c5.vn.application.security.JwtTokenUtil;
import vmo.c5.vn.application.security.JwtUserDetail;
import vmo.c5.vn.application.service.impl.UserService;

@RestController
public class UserController {

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@PostMapping("/user/login")
	public ResponseEntity<UserResponse> createUser(@Valid @RequestBody UserRequestDTO userRequestDTO) {
		JwtUserDetail userDetails;
		try {
			userDetails = (JwtUserDetail) userService.loadUserByUsername(userRequestDTO.getName());
		} catch (UsernameNotFoundException e) {
			logger.debug("error " + e.getMessage());
			userDetails = userService.createUser(userRequestDTO.getName());
		}
		
		final String token = jwtTokenUtil.generateToken(userDetails);
		UserResponse response = new UserResponse(userDetails.getId(), userDetails.getName(), token);
		
		return ResponseEntity.ok(response);
	}

}
