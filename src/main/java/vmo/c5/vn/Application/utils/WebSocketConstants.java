package vmo.c5.vn.application.utils;

public final class WebSocketConstants {
	
	public static final String SIMPLE_BROKER_PATH = "/topic";
	
	public static final String DESTINATION_PREFIX_PATH = "/note-app";
	
	public static final String STOMP_ENPOINT_PATH = "/notification";
	
	public static final String CLIENT_SEND_TO_PATH = "/notification/amount-uncomplete";
	
	public static final String ENDPOINT_CLIENT_PATH = "/topic/notification/amount-uncomplete";
}
