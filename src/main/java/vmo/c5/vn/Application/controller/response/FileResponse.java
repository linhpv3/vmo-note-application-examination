package vmo.c5.vn.application.controller.response;

import lombok.Data;

@Data
public class FileResponse {
	
	private String url;

	private String fileName;

	private String type;
}
