package vmo.c5.vn.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import vmo.c5.vn.application.entity.User;
import vmo.c5.vn.application.repository.UserRepository;
import vmo.c5.vn.application.security.JwtUserDetail;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByName(userName)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with username : " + userName));
		// if not found then create this user
		return JwtUserDetail.create(user);
	}

	public JwtUserDetail createUser(String userName) {
		User user = new User();
		user.setName(userName);
		user = userRepository.save(user);

		return new JwtUserDetail(user.getId(), user.getName());
	}

}
