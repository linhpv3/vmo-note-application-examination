package vmo.c5.vn.application.service;

import org.springframework.web.multipart.MultipartFile;

import vmo.c5.vn.application.controller.response.FileResponse;

public interface UploadFileService {
	public FileResponse uploadImageFile(MultipartFile multipartFile);
}
