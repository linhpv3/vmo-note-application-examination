package vmo.c5.vn.application;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import vmo.c5.vn.application.controller.request.UserRequestDTO;
import vmo.c5.vn.application.security.JwtUserDetail;
import vmo.c5.vn.application.service.impl.UserService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	private ObjectMapper objectMapper;
	
	@BeforeEach
	public void beforeTest() {
		objectMapper = new ObjectMapper();
	}
	
	@MockBean
	UserService userService;
	
	private String API_URL = "/user/login";
	
	@Test
	public void testLoadUserValidName() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String userName = "Pham";
		Integer id = 1;
		Mockito.when(userService.loadUserByUsername(userName))
		.thenReturn(new JwtUserDetail(id, userName));
		
		UserRequestDTO postData = new UserRequestDTO(userName);
		
		mockMvc.perform(post(API_URL)
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(postData))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", Matchers.is(id)))
				.andExpect(jsonPath("$.name", Matchers.is(userName)))
				.andExpect(jsonPath("$.jwToken", Matchers.any(String.class)));
	}

}
