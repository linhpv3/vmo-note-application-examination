
package vmo.c5.vn.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vmo.c5.vn.application.controller.response.FileResponse;
import vmo.c5.vn.application.service.UploadFileService;

@RestController
@RequestMapping("/api/upload/")
public class UploadFileController {

	@Autowired
	private UploadFileService uploadFileService;

	@PostMapping("image")
	public ResponseEntity<FileResponse> uploadFile(@RequestParam("file") MultipartFile multipartFile) {
		return ResponseEntity.ok(uploadFileService.uploadImageFile(multipartFile));
	}
}
