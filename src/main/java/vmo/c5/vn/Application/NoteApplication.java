package vmo.c5.vn.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("vmo.c5.vn.application.repository")
public class NoteApplication {

	public static void main(String[] args) {
		
		System.out.println("file:/" + System.getProperty("user.dir"));
		
		SpringApplication application = new SpringApplication(NoteApplication.class);
		// application.setWebApplicationType(WebApplicationType.NONE);
		application.run(args);
	}
}
