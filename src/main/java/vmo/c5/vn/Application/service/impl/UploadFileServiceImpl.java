package vmo.c5.vn.application.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import vmo.c5.vn.application.controller.response.FileResponse;
import vmo.c5.vn.application.exception.BusinessException;
import vmo.c5.vn.application.service.UploadFileService;
import vmo.c5.vn.application.utils.ImageUtils;

@Service
public class UploadFileServiceImpl implements UploadFileService {

	@Value("${server.port}")
	private int port;

	private Logger logger = LoggerFactory.getLogger(UploadFileServiceImpl.class);

	@Override
	public FileResponse uploadImageFile(MultipartFile multipartFile) {
		String fileName = multipartFile.getOriginalFilename();
		File file = new File(ImageUtils.WORKING_DIR + fileName);

		// check directory is created
		File directory = new File(ImageUtils.WORKING_DIR);
		if (!directory.exists()) {
			directory.mkdirs();
		}

		try {
			multipartFile.transferTo(file);
		} catch (IllegalStateException e) {
			logger.error(e.getMessage());
			throw new BusinessException("Upload file error " + e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new BusinessException("Upload file error " + e.getMessage());
		}

		if (!ImageUtils.isImageFile(file)) {
			throw new BusinessException("File upload is not image file");
		}

		FileResponse fileResponse = new FileResponse();

		fileResponse.setFileName(fileName);
		fileResponse.setType(fileName.substring(fileName.lastIndexOf(".") + 1));
		fileResponse.setUrl(getHTTPUri() + ImageUtils.WEB_PATH_DIR + fileName);
		return fileResponse;
	}

	private String getHTTPUri() {
		String ip = InetAddress.getLoopbackAddress().getHostAddress();
		return "http://" + ip + ":" + port;
	}

}
