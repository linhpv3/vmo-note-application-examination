package vmo.c5.vn.application.dto;

import lombok.Data;
import vmo.c5.vn.application.utils.NoteStatus;

@Data
public class NoteDTO {

	protected Integer Id;

	protected String title;

	protected String description;

	private NoteStatus status;

	protected Integer userId;

	public NoteDTO() {
		super();
	}

	public NoteDTO(String title, String description, NoteStatus status, Integer userId) {
		super();
		this.title = title;
		this.description = description;
		this.userId = userId;
		this.status = status;
	}

	public static NoteDTOBuilder builder(String title, NoteStatus status, Integer userId) {
		return new NoteDTOBuilder(title, status, userId);
	}

	public static class NoteDTOBuilder {

		protected Integer Id;

		protected String title;

		protected String description;

		protected NoteStatus status;

		protected Integer userId;

		public NoteDTOBuilder(String title, NoteStatus status, Integer userId) {
			this.title = title;
			this.status = status;
			this.userId = userId;
		}

		public NoteDTOBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public NoteDTOBuilder withId(Integer Id) {
			this.Id = Id;
			return this;
		}

		public NoteDTO build() {
			return new NoteDTO(this.title, this.description, this.status, this.userId);
		}
	}
}
