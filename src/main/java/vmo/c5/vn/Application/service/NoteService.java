package vmo.c5.vn.application.service;

import java.util.List;

import vmo.c5.vn.application.controller.response.NoteResponse;
import vmo.c5.vn.application.dto.NoteDTO;

public interface NoteService {
	
	NoteResponse getNoteId(Integer noteId);

	List<NoteResponse> getNotes(Integer userId);

	NoteResponse updateNote(NoteDTO noteDTO);

	NoteResponse createNote(NoteDTO noteDTO);
	
	Integer deleteNote(Integer noteId);
	
	Integer amountUncompleteNote(Integer userId);
}
